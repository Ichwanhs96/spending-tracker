export interface SpendingTableContentInterface {
    date: Date,
    category: string,
    amount: number,
}

interface SpendingTableProps {
    contents: SpendingTableContentInterface[]
}

export function SpendingTable(props: SpendingTableProps) {
    return (
        <div className="relative overflow-x-auto shadow-md sm:rounded-lg">
            <table className="w-full text-sm text-left text-white">
                <thead className="text-xs text-white uppercase bg-teal-800">
                    <tr>
                        <th scope="col" className="px-6 py-3">Date</th>
                        <th scope="col" className="px-6 py-3">Category</th>
                        <th scope="col" className="px-6 py-3">Amount</th>
                    </tr>
                </thead>
                <tbody>
                        { props.contents.map((x, index) => 
                            <tr key={ "spending-table-key" + index } className="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                                <td className="px-6 py-4">{ x.date.getFullYear() + "-" + x.date.getMonth() + "-" + x.date.getDate() }</td>
                                <td className="px-6 py-4">{ x.category }</td>
                                <td className="px-6 py-4">{ x.amount }</td>
                            </tr>
                        )}
                </tbody>
            </table>
        </div>
    )
}