'use client'

import { MdFastfood, MdCoffee, MdDirectionsCar, MdSportsEsports, MdSportsSoccer, MdLocalGroceryStore, MdAutoGraph, MdInfo } from "react-icons/md";
import React from "react";

interface SpendingCategory {
    icon: IconType,
    categoryName: string,
}

// import componenets
import { SpendingTable, SpendingTableContentInterface } from "./spending-table"
import { IconType } from "react-icons";
import { useSession, signIn, signOut } from "next-auth/react"

export default function SpendingTracker() {
    const { data: session } = useSession();
    
    const [spendingTableContents, setSpendingTableContents] = React.useState<SpendingTableContentInterface[]>([])
    const [amount, setAmount] = React.useState<number>(0)
    const [categories, setCategories] = React.useState<SpendingCategory[]>([
        {
            icon: MdFastfood,
            categoryName: 'Food'
        },
        {
            icon:  MdCoffee,
            categoryName: 'Coffee'
        },
        {
            icon: MdDirectionsCar,
            categoryName: 'Transportation'
        },
        {
            icon: MdSportsEsports,
            categoryName: 'Entertainment'
        },
        {
            icon: MdSportsSoccer,
            categoryName: 'Sport'
        },
        {
            icon:  MdLocalGroceryStore,
            categoryName: 'Groceries'
        },
        {
            icon:  MdAutoGraph,
            categoryName: 'Investment'
        },
        {
            icon: MdInfo,
            categoryName: 'Other'
        }
    ])
    const [selectedCategory, setSelectedCategory] = React.useState<string>()
    const [selectCategoryError, setSelectCategoryError] = React.useState<boolean>()

    const handleCategoryChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setSelectedCategory(e.target.value)
    }

    const handleAmountChange = (e:React.ChangeEvent<HTMLInputElement>) => {
        setAmount(parseInt(e.target.value));
    }

    const handleAddSpendingButton = (e: React.MouseEvent<HTMLButtonElement>) => {
        if (!selectedCategory) {
            setSelectCategoryError(true)
        } else {
            setSelectCategoryError(false)
            // submit data to database
            // table component to do SSR which access db instead of using props? can this be done? interesting idea
            setSpendingTableContents([...spendingTableContents, {
                date: new Date(),
                category: selectedCategory || 'Other',
                amount: amount
            }])
        }
    }

    return(
        <div>
            { !session ? (
                <div>
                    <button className="flex-shrink-0 border-transparent border-4 bg-teal-500 text-white hover:text-teal-800 text-sm py-1 px-2 rounded" onClick={() => signIn()}>Sign in</button>
                </div>
            ) : (
                <div className="flex justify-center items-center">
                    <div className="w-full max-w-lg">
                        <div>
                            { session.user?.email }
                            <br></br>
                            <button className="flex-shrink-0 border-transparent border-4 bg-gray-500 text-white hover:text-gray-300 text-sm py-1 px-2 rounded" onClick={() => signOut()}>Sign out</button>
                            <ul className={ selectCategoryError ? "flex flex-wrap items-center space-x-2 my-2 p-2 rounded-xl w-full border border-red-500": "flex flex-wrap items-center space-x-2 my-2 p-2 rounded-xl w-full" }>
                                { categories.map((category, index) => {
                                    const Icon = category.icon;
                                
                                    return (
                                        <li key={ "category-key-" + index } className="flex-auto">
                                            <input type="radio" className="peer sr-only" id={ "category-" + index } name="category-selection" value={ category.categoryName } onChange={handleCategoryChange}/>
                                            <label htmlFor={ "category-" + index } className="flex cursor-pointer rounded-xl hover:ring-2 peer-checked:ring-2 ring-teal-500 peer-checked:border-transparent">
                                                <Icon size={48} className="rounded p-2"/>
                                            </label>
                                        </li>
                                    )
                                })}
                            </ul>
                            <p className={ selectCategoryError ? "text-red-500 text-xs italic" : "text-red-500 text-xs italic hidden" }>Please choose a category!</p>
                            <div className="flex items-center border-b border-teal-500 py-2">
                                <input className="appearance-none bg-transparent border-none w-full text-gray-50 py-1 px-2 leading-tight focus:outline-none" type="text" placeholder="e.g. 10000" onChange={handleAmountChange}></input>
                                <button onClick={handleAddSpendingButton} className="flex-shrink-0 border-transparent border-4 bg-teal-500 text-white hover:text-teal-800 text-sm py-1 px-2 rounded">add spending</button>
                            </div>
                            <div className="my-4">
                                <SpendingTable contents={spendingTableContents}/>
                            </div>
                        </div>
                    </div>
                </div>
            )
            }
        </div>
    )
}